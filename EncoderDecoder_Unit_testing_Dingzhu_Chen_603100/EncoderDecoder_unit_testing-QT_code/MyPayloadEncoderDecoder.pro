TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    Dingzhupayloaddecoder.cpp \
    Dingzhupayloadencoder.cpp \
    test_DingzhuPayloadEncoderDecoder.cpp

HEADERS += \
    Dingzhupayloaddecoder.h \
    Dingzhupayloadencoder.h \
    test_DingzhuPayloadEncoderDecoder.h
