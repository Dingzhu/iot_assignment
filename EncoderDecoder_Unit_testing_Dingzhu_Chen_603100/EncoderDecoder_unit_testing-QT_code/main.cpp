/*--------------------------------------------------------------------
main.cpp file used to do the encoder-decoder unit testing
  --------------------------------------------------------------------*/

/*!
 * \file main.cpp
 * \brief Unit test to test and develop the payload -encoder -decoder.
 * \author Dingzhu Chen (Dingzhu Chen (D.Chen1@student.han.nl))
 * \student number 603100
 * \date See version table
 * \version see version table
 *
 * # Version history
 *
 * Version|Date        |Note
 * -------|------------|-----------------
 * 0.1    | 18-4-2021  | Initial version
 *
 *
 */

#include <iostream>

#include "Dingzhupayloaddecoder.h"
#include "Dingzhupayloadencoder.h"
#include "test_DingzhuPayloadEncoderDecoder.h"

using namespace std;

int main()
{
    // Run unit tests
    cout << "Unit test Payload -encoder -decoder." << endl;
    outputTest();
    cout << "Unit tests executed." << endl;
    return 0;
}
