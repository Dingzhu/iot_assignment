/*--------------------------------------------------------------------
  This file is the member function implementations relating to the unit testing
  --------------------------------------------------------------------*/
/*!

# Description of unit test.
The unit test is containing one function named outputTest(){}.
The output Test is a small test function that test a specific feature of the payload
-encoder -decoder class.

The main purpose of this outputTest(){}function is that to test if the encoder and decoder class objects can transport and store the sensor data with highest resolution.
Tests are using the following schematic:

\verbatim

                   +---------+                   +---------+
                   |         |                   |         |
input variable --->| encoder |----> payload ---->| decoder |----> result
                   |         |                   |         |
                   +---------+                   +---------+

\endverbatim

Data channel| type(bits)| value | comment
------------|-----------|-------|----------------
1           | 103       | 67.9  | 0.1°C Signed MSB, temperature sensor
1           | 104       | 29.5  | 0.5 % Unsigned, Humidity sensor


 */
#include <iostream>

#include "Dingzhupayloaddecoder.h"
#include "Dingzhupayloadencoder.h"
#include "test_DingzhuPayloadEncoderDecoder.h"

using namespace std;
#define LPP_CH_ADC0VOLTAGE      88    ///< CayenneLPP CHannel for ADC input 1 (potmeter 1)
#define LPP_CH_ADC1VOLTAGE      11    ///< CayenneLPP CHannel for ADC input 2 (potmeter 2)

void outputTest()
{
    // Test 1: known input 1
    DingzhuPayloadEncoder encoder(SENSOR_PAYLOAD_SIZE);   /// Encoder object
    DingzhuPayloadDecoder decoder;    ///<Decoder object
    float temperature = 67.3;    ///< Test variable containing value that shall be transferred
    float humidity = 29.5;          ///< Test variable containing value that shall be transferred
    encoder.settemperature(temperature);             // Set variable in encoder object.
    encoder.sethumidity(humidity);             // Set variable in encoder object.

    encoder.addtem();
    encoder.addhum();

    uint8_t *payloadBuffer = encoder.getBuffer();
    uint8_t payloadSize = encoder.getSize();

    decoder.setPayload(payloadBuffer);//set pointer to decode class buffer containing the payload
    decoder.setPayloadSize(payloadSize);
    decoder.decodePayload();//extract sensor data from buffer and assign it to a given decoder class variable
    if(decoder.gettempFlag()){
        cout<<"temperature="<<decoder.getfinalTemperature()<<endl;// Retrieve temperature from decoder object and cout it.
    }
    if(decoder.gethumidFlag()){
        cout<<"humidity="<<decoder.getfinalHumidity()<<endl;
    }
}






