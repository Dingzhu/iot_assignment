/*--------------------------------------------------------------------
  This file is the member function implementation file of the decoder class
  --------------------------------------------------------------------*/

#include "Dingzhupayloaddecoder.h"
/// \brief constructor
DingzhuPayloadDecoder::DingzhuPayloadDecoder():
    _buffer{nullptr}
{

}
/// \brief destructor
DingzhuPayloadDecoder::~DingzhuPayloadDecoder()
{

}

/// \brief extract sensor data from buffer and assign it to a given
/// decoder class variable
///.
/// also determine which type of sensor data by comparing the type number on
/// second byte of CayenneLPP schema data
void DingzhuPayloadDecoder::decodePayload()
{
    _rawTemperature = static_cast<int>(extract_uint16(_buffer, 2));
    _rawHumidity = static_cast<int>(extract_uint8(_buffer, 6));
    if (_buffer[1]=103){
        settempFlag(1);
    }
    else settempFlag(0);
    if (_buffer[5]=104){
        sethumidFlag(1);
    }
    else sethumidFlag(0);
}



/// \brief extract uint8 from payload at given position
/// \param *buf pointer to buffer with payload
/// \param idx start location in _buffer of uint16
/// \return value read from buffer
uint8_t DingzhuPayloadDecoder::extract_uint8 (const uint8_t *buf, const unsigned char idx){
    return (uint8_t)buf[idx];
}

/// \brief extract uint16 from payload at given position
/// \param *buf pointer to buffer with payload
/// \param idx start location in _buffer of uint16
/// \return value read from buffer
uint16_t DingzhuPayloadDecoder::extract_uint16(const uint8_t *buf, const unsigned char idx){
    uint16_t value {0};
    value  = ((uint16_t)buf[idx] << 8);  // msb
    value |=  (uint16_t)buf[idx+1];      // lsb
    return value;
}

/// \brief extract uint32 from payload at given position
/// \param *buf pointer to buffer with payload
/// \param idx start location in _buffer of uint16
/// \return value read from buffer
uint32_t DingzhuPayloadDecoder::extract_uint32(const uint8_t *buf, const unsigned char idx){
    uint32_t value {0};
    for (uint8_t i=0; i<4; i++) {
        value |= ((uint32_t)buf[idx+i] << (24-(i*8)));  // msb
    }
    return value;
}

/// \brief extract uint64 from payload at given position
/// \param *buf pointer to buffer with payload
/// \param idx start location in _buffer of uint16
/// \return value read from buffer
uint64_t DingzhuPayloadDecoder::extract_uint64(const uint8_t *buf, const unsigned char idx){
    uint64_t value {0};
    for (uint8_t i=0; i<8; i++) {
        value |= ((uint64_t)buf[idx+i] << (56-(i*8)));  // msb
    }
    return value;
}
