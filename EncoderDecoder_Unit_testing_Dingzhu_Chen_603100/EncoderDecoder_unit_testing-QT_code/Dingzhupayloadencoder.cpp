/*--------------------------------------------------------------------
  This file is the member function implementation file of the encoder class
  --------------------------------------------------------------------*/

#include "Dingzhupayloadencoder.h"
#include <stdlib.h> // malloc()
using namespace std;

DingzhuPayloadEncoder::DingzhuPayloadEncoder(uint8_t size) : maxsize(size)
{
    buffer = (uint8_t *)malloc(size);
    cursor = 0;
}

DingzhuPayloadEncoder::~DingzhuPayloadEncoder(void)
{
    free(buffer);
}

void DingzhuPayloadEncoder::reset(void)
{
    cursor = 0;
}

uint8_t DingzhuPayloadEncoder::getSize(void)
{
    return cursor;
}

uint8_t *DingzhuPayloadEncoder::getBuffer(void)
{
    return buffer;
}
/// \brief transport temperature value into a private member variable
/// and increase the cursor value.
void DingzhuPayloadEncoder::addtem()
{
    cursor = 0;              // init
    cursor = addTemperature(LPP_CH_ADC0VOLTAGE, _temperatureEncode);
}
/// \brief transport humidity value into a private member variable
/// and increase the cursor value.
void DingzhuPayloadEncoder::addhum()
{
    cursor = addRelativeHumidity(LPP_CH_ADC1VOLTAGE, _humidityEncode);
}
/// \brief transport temperature value into the buffer
/// \param channel is a ramdom channel chosen by temperature sensor
/// \param celsius is variable containning the value of the temperature sensor
/// \return cursor, the byte number of buffer that used to store sensor data
uint8_t DingzhuPayloadEncoder::addTemperature(uint8_t channel, float celsius)
{
    if ((cursor + LPP_TEMPERATURE_SIZE) > maxsize)
    {
        return 0;
    }
    int16_t val = celsius * 10;
    buffer[cursor++] = (channel);
    buffer[cursor++] = (LPP_TEMPERATURE)&0xFF;
    buffer[cursor++] = (val >> 8);
    buffer[cursor++] = (val);
    return cursor;
}

/// \brief transport humidity value into the buffer
/// \param[in] channel is a ramdom channel chosen by humidity sensor
/// \param rh is variable containning the value of the humidity sensor
/// \return cursor, the byte number of buffer that used to store sensor data
uint8_t DingzhuPayloadEncoder::addRelativeHumidity(uint8_t channel, float rh)
{
    if ((cursor + LPP_RELATIVE_HUMIDITY_SIZE) > maxsize)
    {
        return 0;
    }
    buffer[cursor++] = channel;
    buffer[cursor++] = LPP_RELATIVE_HUMIDITY;
    buffer[cursor++] = rh*2;
    return cursor;
}


