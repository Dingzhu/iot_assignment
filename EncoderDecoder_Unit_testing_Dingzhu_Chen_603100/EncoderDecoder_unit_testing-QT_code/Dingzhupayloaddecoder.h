/*--------------------------------------------------------------------
  This file is the class decleration the decoder class consists of its
  attributes and methods
  --------------------------------------------------------------------*/

/*!
 * \file Dingzhupayloaddecoder.h
 * \brief payloaddecoder class to decode data using CayenneLPP schema
 * \author Dingzhu Chen (Dingzhu Chen (D.Chen1@student.han.nl))
 * \student number 603100
 * \date 18-4-2021
 * \version see version table
 *
 * # Version history
 *
 * Version|Date        |Note
 * -------|------------|-----------------
 * 0.1    | 18-4-2021  | Initial version
 */

#ifndef Dingzhu_PAYLOAD_DECODER_H
#define Dingzhu_PAYLOAD_DECODER_H

#include <stdint.h> // uint8_t, uint16_t, and uint32_t type

const uint8_t VALUE_1 = 0;
const uint8_t VALUE_2 = 4;

/// \brief payload decode class
/// This class wil decode variables out of a payload for use in a LoRaWAN application.
/// The class is setup using both .h and .cpp files where the setters and getters are
/// placed in to the .h file.
class DingzhuPayloadDecoder
{
private:
    uint8_t *_buffer;           ///< buffer containing payload with sensor data
    uint8_t _bufferSize;        ///< Size of payload for housekeeping.

    uint32_t _rawTemperature;     ///< demo variable, 32 bits sized, used to store data from buffer
    uint32_t _rawHumidity;     ///< demo variable, 16 bits sized
    bool _temperatureFlag ;        ///< a variable to indicate iF the data is from temperature sensor
    bool _humidityFlag;           ///< a variable to indicate iF the data is from humidity sensor
    /// \brief Get uint32_t type number from payload at given position
    /// \param buf Buffer containing payload
    /// \param idx position in buffer at which number is starting
    /// \return number
    uint64_t extract_uint64(const uint8_t *buf, const unsigned char idx = 0);
    /// \brief Get uint32_t type number from payload at given position
    /// \param buf Buffer containing payload
    /// \param idx position in buffer at which number is starting
    /// \return number
    uint32_t extract_uint32(const uint8_t *buf, const unsigned char idx = 0);

    /// \brief Get uint16_t type number from payload at given position
    /// \param buf Buffer containing payload
    /// \param idx position in buffer at which number is starting
    /// \return number
    uint16_t extract_uint16(const uint8_t *buf, const unsigned char idx = 0);

    /// \brief Get uint8_t type number from payload at given position
    /// \param buf Buffer containing payload
    /// \param idx position in buffer at which number is starting
    /// \return number
    uint8_t extract_uint8 (const uint8_t *buf, const unsigned char idx = 0);

public:
    DingzhuPayloadDecoder();     ///< Constructor
    ~DingzhuPayloadDecoder();    ///< Destuctor
    void settempFlag(uint16_t value){_temperatureFlag=value;}
    void sethumidFlag(uint16_t value){_humidityFlag=value;}

    int gettempFlag(){return _temperatureFlag;}
    int gethumidFlag(){return _humidityFlag;}


    /// \brief set pointer to buffer containing the payload
    /// \param *payload
    void setPayload(uint8_t *payload){_buffer = payload;}

    /// \brief set size of payload.
    /// \param size of payload in bytes.
    void setPayloadSize(uint8_t size){_bufferSize = size;}

    /// \brief decode payload and put individual values in member variables.
    /// \pre payload and size shall be set.
    void decodePayload();

    /// \brief "Getter" to demonstrate getting the value of a private member variable
    /// return The value of the variable
    double  getTemperature() const {return _rawTemperature;}//watch out the type!!!

    double  getfinalTemperature() {return getTemperature()/10;}//watch out the type!!!

    double  getHumidity() const {return _rawHumidity;}//watch out the type!!!

    double  getfinalHumidity() {return getHumidity()/2;}//watch out the type!!!

    int  getbufferSize(){return _bufferSize;}

    uint8_t *getBuffer(void)
    {
        return _buffer;
    }
};

#endif // Dingzhu_PAYLOAD_DECODER_H
