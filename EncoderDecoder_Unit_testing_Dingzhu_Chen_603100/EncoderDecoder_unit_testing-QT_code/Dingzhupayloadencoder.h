/*--------------------------------------------------------------------
  This file is the class decleration the encoder class consists of its
  attributes and methods
  --------------------------------------------------------------------*/

/*!
 * \file Dingzhupayloadencoder.h
 * \brief payloadencoder class using CayenneLPP schema
 * \author Dingzhu Chen (Dingzhu Chen (D.Chen1@student.han.nl))
 * \student number 603100
 * \date See version table
 * \version see version table
 *
 * # Version history
 *
 * Version|Date        |Note
 * -------|------------|-----------------
 * 0.1    | 18-4-2021  | Initial version
 *
 */

#ifndef DingzhuPAYLOADENCODER_H
#define DingzhuPAYLOADENCODER_H
#include <stdint.h> // uint8_t, uint16_t, and uint32_t type
//

#define LPP_CH_ADC0VOLTAGE              88    ///< CayenneLPP CHannel for ADC input 1 (potmeter 1)
#define LPP_CH_ADC1VOLTAGE              07    ///< CayenneLPP CHannel for ADC input 2 (potmeter 2)
#define LPP_TEMPERATURE                 103   // 2 bytes, 0.1°C signed
#define LPP_TEMPERATURE_SIZE            4    // 2 bytes, 0.1°C signed
#define LPP_RELATIVE_HUMIDITY           104
#define LPP_RELATIVE_HUMIDITY_SIZE      3


const uint8_t SENSOR_PAYLOAD_SIZE = 52;     ///< Payload size for sensor

/// \brief payload endoder class
/// This class wil encode variables for the LoRaWAN application in to a single payload
/// The class is setup using both .h and .cpp files where the setters and getters are
/// placed in to the .cpp file.
class DingzhuPayloadEncoder
{
private:
    float _temperatureEncode;     ///< a float variable used to store temperature data from buffer
    float _humidityEncode;         ///< a float variable used to store humidity data from buffer

    uint8_t *buffer;              ///< buffer containing payload with sensor data
    uint8_t maxsize;
    uint8_t cursor;              ///< index
    /// \brief add uint32 to payload
    /// \param idx_in start location in _buffer
    /// \param value uint32_t value
    /// \return First free location at which new dat acan be stored in _buffer
    //    unsigned char add_uint32 (unsigned char idx_in, uint32_t value);

    /// \brief add uint16 to payload
    /// \param idx_in start location in _buffer
    /// \param value uint16_t value
    /// \return First free location at which new dat acan be stored in _buffer
    //    unsigned char add_uint16 (unsigned char idx_in, const uint16_t value);

public:
    DingzhuPayloadEncoder(uint8_t size);     ///< Constructor
    ~DingzhuPayloadEncoder();    ///< Destructor

    void reset(void);
    uint8_t getSize(void);
    uint8_t *getBuffer(void);
    uint8_t copy(uint8_t *buffer);

    uint8_t addTemperature(uint8_t channel, float celsius);//potmeter1

    uint8_t addRelativeHumidity(uint8_t channel, float rh);

    /// \brief "Setter" to demonstrate setting a private member variable
    /// \param[in] var variable of the temperature sensor
    void settemperature(const float var){_temperatureEncode = static_cast<float>(var);}

    /// \brief "Setter" to demonstrate setting a private member variable
    /// \param[in] var variable of the humidity sensor
    void sethumidity(const float var){_humidityEncode = static_cast<float>(var);}

    void addtem();

    void addhum();
    /// \brief get payload size.
    /// (this is a "Getter")
    /// \return buffer size in bytes
    //    uint8_t getPayloadSize(){return _bufferSize;};

    /// \brief get payload.
    /// (this is a "Getter")
    /// \return pointer to buffer
    //    uint8_t *getPayload(){return _buffer;};
};

#endif // DingzhuPAYLOADENCODER_H
