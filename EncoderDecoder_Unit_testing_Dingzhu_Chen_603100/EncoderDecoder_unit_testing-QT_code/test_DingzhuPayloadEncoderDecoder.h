/*--------------------------------------------------------------------
  This file is the member function declerations relating to the unit testing
  --------------------------------------------------------------------*/

/*!
 * \file test_DingzhuPayloadEncoderDecoder.h
 * \brief unit tests to test payloadencoder and payload decoder classes.
 * \author Dingzhu Chen (D.Chen1@student.han.nl)
 * \student number 603100
 * \date 18-4-2021
 * \version see version table
 *
 * # Version history
 *
 * Version|Date        |Note
 * -------|------------|-----------------
 * 0.1    | 18-4-2021  | Initial version
 *
 *
 */

/*!

# Description of unit test.
The unit test is containing one function named outputTest(){}.
The output Test is a small test function that test a specific feature of the payload
-encoder -decoder class.

The main purpose of this outputTest(){}function is that to test if the encoder and decoder class objects can transport and store the sensor data with highest resolution.
Tests are using the following schematic:


\verbatim

                   +---------+                   +---------+
                   |         |                   |         |
input variable --->| encoder |----> payload ---->| decoder |----> result
                   |         |                   |         |
                   +---------+                   +---------+

\endverbatim

Data channel| type(bits)| value | comment
------------|-----------|-------|----------------
1           | 103       | 67.9  | 0.1°C Signed MSB, temperature sensor
1           | 104       | 29.5  | 0.5 % Unsigned, Humidity sensor


 */

#ifndef TEST_DingzhuPAYLOADENCODERDECODER_H
#define TEST_DingzhuPAYLOADENCODERDECODER_H

/// \brief function to do unit test
void outputTest();

#endif // TEST_DingzhuPAYLOADENCODERDECODER_H
